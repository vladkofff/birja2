import React from 'react';
import './App.css';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import AdminComponent from './components/AdminComponent';
import MainComponent from './components/MainComponent';

function App() {
  return (
    
      <BrowserRouter>
        <Switch>
          <Route path="/admin" component={() => <AdminComponent />} />
          <Route path="/" component={() => <MainComponent />} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;
