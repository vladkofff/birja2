import React, { useState, useEffect } from 'react';

const backURL = "/api";


function AdminComponent() {
    const [users, setUsers] = useState([]);
    const [userStocks, setUserStocks] = useState([]);


    useEffect(() => {
        refreshUsers(setUsers);
    }, [])

    return (
        <div className="row">
            <div>
                <div>Click on name to watch user's stocks</div>
                {users.map(user => {
                    return (<div onClick={setNewStocks(user.name, setUserStocks)}>{user.name}({user.money})</div>)
                })}
            </div>
            <div>
                {console.log(userStocks)}
                {userStocks.map(userStock => {
                    console.log(userStock);
                    return (<div>{userStock.stock.name} - amount {userStock.amount} - cost {userStock.stock.cost}</div>)   
                })}
            </div>
        </div>
    )
}

const refreshUsers = async (setUsers) => {
    fetch(`${backURL}/users`).then(res => res.json()).then(users => users.map((user, index) => ({ ...user, key: index }))).then(setUsers)
}

const setNewStocks = (name, setUserStocks) => {
    return async () => {
        fetch(`${backURL}/users/stocks?name=${name}`).then(res => res.json()).then(res => res.map((i, index) => ({ ...i, key: index }))).then(setUserStocks);
    }
}

export default AdminComponent