import React, {useState, useEffect} from 'react';

const backURL = "/api";

function MainComponent() {
    const [users, setUsers] = useState([]);
    const [stocks, setStocks] = useState([]);
    const [userStocks, setUserStocks] = useState([]);
    const [name, setName] = useState(null);

    useEffect(() => {
        refreshUsers(setUsers);
    }, [])

    return (
        <div>
            <div>
                <select id="names" onChange={changeUserStockBuilder(setUserStocks, setStocks, setName)}>
                    {users.map(user => {
                        return (<option className="options" value={user.name}>{user.name}</option>)
                    })}
                </select>
            </div>
            <div>
                {stocks.map(stock => {
                return (<div>{stock.name} - cost {stock.cost} - amount {stock.amount}
                 <form method="post" onSubmit={buildBuyForm(setUserStocks, setStocks, setName, stock.name, name)}>
                    <input type="number" name="amount" placeholder="Amount"/>
                    <input type="submit" value="Buy"/>
                </form>
                    </div>)
                })}
            </div>
            <div>
                {userStocks.map(userStocks => {
                return (<div>{userStocks.stockName} - cost {userStocks.stock.cost} - amount {userStocks.amount}
                 <form method="post" onSubmit={buildSellForm(setUserStocks, setStocks, setName, userStocks.stock.name, name)}>
                    <input type="number" name="amount" placeholder="Amount"/>
                    <input type="submit" value="Sell"/>
                </form>
                    </div>)
                })}
            </div>
        </div>
    )
}

const buildBuyForm = (setUserStocks, setStocks, setName, stockName, name) => {
    return async (e) => {
        e.preventDefault();
                
        await fetch(`${backURL}/buy`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },  
            body: JSON.stringify({
                name, stockName, amount: Number(e.target.amount.value)
            })
        })
        changeUserStockBuilder(setUserStocks, setStocks, setName)({target: {value: name}});
    } 
} 

const buildSellForm = (setUserStocks, setStocks, setName, stockName, name) => {
    return async (e) => {
        e.preventDefault();
        
        await fetch(`${backURL}/sell`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },  
            body: JSON.stringify({
                name, stockName, amount: Number(e.target.amount.value)
            })
        })
        changeUserStockBuilder(setUserStocks, setStocks, setName)({target: {value: name}});
    } 
} 

const changeUserStockBuilder = (setUserStocks, setStocks, setName) => {
    return (e) => {
        setName(e.target.value)
        setNewStocks(e.target.value, setUserStocks)()
        refreshStocks(setStocks);
    }
}

const refreshUsers = async (setUsers) => {
    fetch(`${backURL}/users`).then(res => res.json()).then(users => users.map((user, index) => ({ ...user, key: index }))).then(setUsers)
}

const refreshStocks = async (setStocks) => {
    fetch(`${backURL}/stocks`).then(res => res.json()).then(stocks => stocks.map((stock, index) => ({ ...stock, key: index }))).then(setStocks)
}

const setNewStocks = (name, setUserStocks) => {
    return async () => {
        fetch(`${backURL}/users/stocks?name=${name}`).then(res => res.json()).then(res => res.map((i, index) => ({ ...i, key: index }))).then(setUserStocks);
    }
}
export default MainComponent