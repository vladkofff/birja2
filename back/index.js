const express = require('express')
const path = require('path');

const PORT = process.env.PORT || 4200
const bodyParser = require('body-parser')
const cors = require('cors')

const db = require("./db.json");

const users = db.users
const stocks = db.stocks
const stockUser = []

express()
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(cors())
  .get("/api/users", (req, res) => {
    res.json(Object.keys(users).map(name => ({name, ...users[name]})));
  })
  .get("/api/stocks", (req, res) => {
    res.json(Object.keys(stocks).map(name => ({name, ...stocks[name]})));
  })
  .post("/api/buy", (req, res) => {
    const {name, stockName, amount} = req.body;

    const targetStock = stocks[stockName];
    const targetUser = users[name];
    const isEnoughMoney = targetUser.money >= targetStock.cost * amount;
    const isEnoughStocks = targetStock.amount >= amount

    if (!isEnoughMoney || !isEnoughStocks) {
      return res.status(400).json({
        errors: {isEnoughMoney, isEnoughStocks}
      })
    } else {
      const stockUserConnection = stockUser.find(i => i.name === name && i.stockName === stockName);
    
      if (stockUserConnection) {
        stockUserConnection.amount += amount;
      } else {
        stockUser.push({
          name, stockName, amount
        })
      }
      const user = users[name];

      targetUser.money -= targetStock.cost * amount;
      targetStock.amount -= amount;

      res.json({})
    }
  })
  .post("/api/sell", (req, res) => {
    const {name, stockName, amount} = req.body;

    const stockUserConnection = stockUser.find(i => i.name === name && i.stockName === stockName);
    const user = users[name];
    const stock = stocks[stockName];

    const isEnoughStocks = stockUserConnection && stockUserConnection.amount >= amount;

    if(!isEnoughStocks) {
      res.status(400).json({isEnoughStocks})
    } else {
      stockUserConnection.amount -= amount;
      user.money += stock.cost * amount; 
      stock.amount += amount;
      res.json({})
    }
  })
  .get("/api/users/stocks", (req, res) => {
    const {name} = req.query;

    const userStocks = stockUser.filter(i => i.name === name)

    res.json(userStocks.map(userStock => ({...userStock, stock: {...stocks[userStock.stockName], name: userStock.stockName}})))
  })
  .use(express.static(__dirname + '/build'))
  .get('/*', (req, res) => res.sendFile(path.join(__dirname, 'build', 'index.html')))
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))